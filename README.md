# GathrUp VueJS backend application

# Add environment variables to your application
MongoDB:   
 - mongoDBHost=Your_mongodb_host_url  
 - mongoDBPort=Your_mongodb_port  
 - mongoDBAuthDatabase=Your_mongodb_Authentication_database  
 - mongoDBUsername=Your_mongodb_username  
 - mongoDBPassword=Your_mongodb_password  
 - mongoDBDatabase=Your_mongodb_database  
 
Email:  
 - mailHost=Your_mail_host (example: smtp.gmail.com)  
 - mailUsername=Your_system_email  
 - mailPassword=Your_system_mail_password  
 - mailPort=Your_mail_port  
 - mailAuth=true  
 
 Frontend:  
 frontendURL=Your_frontend_url  
 
Required:  
authMessages=Your_path_to_messegaes.properties  

# Messages.properties
Used for getting the error messages for passwords  

INSUFFICIENT_UPPERCASE=Wachtwoord moet minimaal %1$s hoofletter(s) bevatten.  
INSUFFICIENT_LOWERCASE=Wachtwoord moet minimaal %1$s normale letter bevatten.  
INSUFFICIENT_DIGIT=Wachtwoord moet minimaal %1$s cijfer bevatten.  
INSUFFICIENT_SPECIAL=Wachtwoord moet minimaal %1$s speciaal teken bevatten.  
TOO_LONG=Wachtwoord mag maximaal %2$s karakters bevatten zijn.  
TOO_SHORT=Wachtwoord moet minimaal %1$s karakters bevatten.  
HISTORY_VIOLATION=Password matches one of %1$s previous passwords.  
ILLEGAL_WORD=Password contains the dictionary word '%1$s'.  
ILLEGAL_WORD_REVERSED=Password contains the reversed dictionary word '%1$s'.  
ILLEGAL_MATCH=Password matches the illegal pattern '%1$s'.  
ALLOWED_MATCH=Password must match pattern '%1$s'.  
ILLEGAL_CHAR=Password contains the illegal character '%1$s'.  
ALLOWED_CHAR=Password contains the illegal character '%1$s'.  
ILLEGAL_SEQUENCE=Password contains the illegal sequence '%1$s'.  
ILLEGAL_USERNAME=Password contains the user id '%1$s'.  
ILLEGAL_USERNAME_REVERSED=Password contains the user id '%1$s' in reverse.  
ILLEGAL_WHITESPACE=Wachtwoord mag geen spaties bevatten.  
INSUFFICIENT_ALPHABETICAL=Password must contain at least %1$s alphabetical characters.  
INSUFFICIENT_CHARACTERISTICS=Password matches %1$s of %3$s character rules, but %2$s are required.  
SOURCE_VIOLATION=Password cannot be the same as your %1$s password.  

