package com.sandervanderburgt.eventplannerfinal.todo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sandervanderburgt.eventplannerfinal.invite.Invite;
import com.sandervanderburgt.eventplannerfinal.invite.InviteData;
import com.sandervanderburgt.eventplannerfinal.request.AllGivenTodosBody;
import com.sandervanderburgt.eventplannerfinal.request.TodoItemUpdateBody;
import com.sandervanderburgt.eventplannerfinal.user.UserData;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TodoController {

    @ApiOperation("Creates new to do item for the given event with the given values")
    @CrossOrigin
    @RequestMapping(value = "/todo/new", method = RequestMethod.POST, produces = "application/json")
    public String newTodoItem(@Valid @RequestBody Todo todoItem, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // check if any of the fields are empty
        if(StringUtils.isAnyEmpty(todoItem.getTitle())){
            returnObject.put("error", true);
            errorList.add("Één of meerdere velden zijn leeg gelaten! Probeer het A.U.B. opnieuw");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            returnObject.put("result", TodoData.newToDoItem(todoItem));
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Creates new to do item for the given event with the given values")
    @CrossOrigin
    @RequestMapping(value = "/todo/get/by_event_id/{eventID}", method = RequestMethod.GET, produces = "application/json")
    public String getTodoitemsByEventID(@PathVariable("eventID") ObjectId eventID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if eventID is 24 characters long
        if(eventID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Create new list for the invites
            List<Object> todoItems = new ArrayList<>();

            // Loop through all todoItems of event and add to new List
            for (Todo todoItem : TodoData.getAllTodoItemsByEventID(eventID)) {
                todoItems.add(todoItem.getFullToDoItem());
            }
            returnObject.put("result", todoItems);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Updates all todo items to the given values in the array using the _id")
    @CrossOrigin
    @RequestMapping(value = "/todo/put/given_todo_items", method = RequestMethod.PUT, produces = "application/json")
    public String UpdateAllGivenTodoItems(@RequestBody AllGivenTodosBody todos, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        if(todos.getTodo().size() > 0){
            for (Todo todoItem: todos.getTodo()) {

                // Check if eventID is 24 characters long
                if(todoItem.get_id().toString().length() != 24){
                    returnObject.put("error", true);
                    errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
                    returnObject.put("errormsg", errorList);
                    return new JSONObject(returnObject).toString();
                }
                TodoData.UpdateTodoItem(todoItem);
            }
        }
        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Updates all todo items to the given values in the array using the _id")
    @CrossOrigin
    @RequestMapping(value = "/todo/delete/by_id/{todoID}", method = RequestMethod.DELETE, produces = "application/json")
    public String deleteTodoItem(@PathVariable("todoID") ObjectId todoID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if eventID is 24 characters long
        if(todoID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        if(!((Boolean) returnObject.get("error"))) {
            Todo todoItem = TodoData.getTodoItemByID(todoID);
            TodoData.deleteTodoItem(todoItem);
        }
        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Update a single todo item using it's id")
    @CrossOrigin
    @RequestMapping(value = "/todo/put/by_id/{todoID}", method = RequestMethod.PUT, produces = "application/json")
    public String updateTodoItem(@PathVariable("todoID") ObjectId todoID, @RequestBody TodoItemUpdateBody todoItem, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if eventID is 24 characters long
        if(todoID.toString().length() != 24 && !StringUtils.isAnyEmpty(todoItem.getTitle(), todoItem.getDescription())){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        if(!((Boolean) returnObject.get("error"))) {

            Todo original = TodoData.getTodoItemByID(todoID);
            original.setTitle(todoItem.getTitle());
            original.setDescription(todoItem.getDescription());
            TodoData.updateTodoItem(original);

            returnObject.put("result", original.getFullToDoItem());
        }
        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }
}
