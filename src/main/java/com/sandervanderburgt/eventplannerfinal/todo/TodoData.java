package com.sandervanderburgt.eventplannerfinal.todo;

import com.sandervanderburgt.eventplannerfinal.invite.InviteController;
import com.sandervanderburgt.eventplannerfinal.repositories.TodoRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TodoData {

    private static TodoRepository repository;

    @Autowired
    public void setRepository(TodoRepository repository) {
        TodoData.repository = repository;
    }

    public static Map<String, Object> newToDoItem(Todo toDo){
        toDo.set_id(ObjectId.get());
        repository.save(toDo);
        return toDo.getFullToDoItem();
    }

    public static Map<String, Object> updateTodoItem(Todo todo){
        repository.save(todo);
        return todo.getFullToDoItem();
    }

    public static Todo getTodoItemByID(ObjectId todoItemID){
        return repository.findBy_id(todoItemID);
    }

    public static void deleteTodoItem(Todo todoItem){
        repository.delete(todoItem);
    }

    public static List<Todo> getAllTodoItemsByEventID(ObjectId eventID){
        return repository.findAllByEventID(eventID);
    }

    public static void UpdateTodoItem(Todo todoItem){
        repository.save(todoItem);
    }

}
