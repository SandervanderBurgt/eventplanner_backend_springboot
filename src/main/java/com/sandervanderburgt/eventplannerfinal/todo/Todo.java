package com.sandervanderburgt.eventplannerfinal.todo;

import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.Map;

public class Todo {
    @Id
    private ObjectId _id;

    private ObjectId eventID;
    private String title, description;
    private int status, index;

    public Todo() {
    }

    public Todo(ObjectId _id, ObjectId eventID, String title, String description, int status, int index) {
        this._id = _id;
        this.eventID = eventID;
        this.title = title;
        this.description = description;
        this.status = status;
        this.index = index;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public ObjectId getEventID() {
        return eventID;
    }

    public void setEventID(ObjectId eventID) {
        this.eventID = eventID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @ApiModelProperty(hidden = true)
    public Map<String, Object> getFullToDoItem() {
        Map<String, Object> toDoItemMap = new HashMap<>();
        toDoItemMap.put("_id", this._id.toString());
        toDoItemMap.put("eventID", this.eventID.toString());
        toDoItemMap.put("title", this.title);
        toDoItemMap.put("description", this.description);
        toDoItemMap.put("status", this.status);
        toDoItemMap.put("index", this.index);
        return toDoItemMap;
    }
}
