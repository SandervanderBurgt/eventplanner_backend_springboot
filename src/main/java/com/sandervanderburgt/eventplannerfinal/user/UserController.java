package com.sandervanderburgt.eventplannerfinal.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sandervanderburgt.eventplannerfinal.MailService;
import com.sandervanderburgt.eventplannerfinal.token.Token;
import com.sandervanderburgt.eventplannerfinal.token.TokenData;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class UserController {

    @Autowired
    MailService mailService;

    @Value("${GathrUp.frontend.url}")
    String frontendUrl;;

//    @RequestMapping(value = "/user/{action}/{ownID}", method = RequestMethod.POST, produces = "application/json")
//    public String className(@PathVariable("action") String action, @PathVariable("ownID") ObjectId ownID, @Valid @RequestBody(required = false) String body) throws JSONException, JsonProcessingException {
//        Map<String, Object> returnObject = new HashMap<>();
//        returnObject.put("error", false);
//        returnObject.put("errormsg", "");
//
//        switch (action) {
////            case "getall":
////                List<Object> users = new ArrayList<>();
////
////                for (int i = 0; i < UserData.getAllUsers().size(); i++) {
////                    users.add(UserData.getAllUsers().get(i).getMap());
////                }
////
////                returnObject.put("result", users);
////                returnObject.put("result-size", 5);
////                return new JSONObject(returnObject).toString();
//        }
//    }


    @ApiOperation("Returns single user by giving it's ID")
    @CrossOrigin
    @RequestMapping(value = "/user/get/by_id/{id}", method = RequestMethod.GET, produces = "application/json")
    public String GetUserByID(@PathVariable("id") ObjectId gottenID) throws JSONException, JsonProcessingException {
        // Use standard return template
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");

        // parse ID from URL
        String parsedID = gottenID.toString();

        // check if ID is correct ID, if not return error with invalid ID message
        if(parsedID.length() != 24){
            returnObject.put("error", true);
            returnObject.put("errormsg", "Invalid ID");
            return new JSONObject(returnObject).toString();
        }
        // convert parsedID to ObjectID
        ObjectId objectId = new ObjectId(parsedID);

        // Check if userID returns user, if so return full user
        if(UserData.getUserByID(objectId) != null){
            User gottenUser = UserData.getUserByID(objectId);
            returnObject.put("result", gottenUser.getFullUser());
            return new JSONObject(returnObject).toString();
        }

        // If no user found, return null
        returnObject.put("result", null);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Returns single user by giving it's email")
    @CrossOrigin
    @RequestMapping(value = "/user/get/by_email/{email}", method = RequestMethod.GET, produces = "application/json")
    public String GetUserByEmail(@PathVariable("email") String gottenEmail) throws JSONException, JsonProcessingException {
        // Use standard return template
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");

        // Check if email is set, if so return the full user
        if(UserData.getUserByEmail(gottenEmail) != null){
            User gottenUser = UserData.getUserByEmail(gottenEmail);
            returnObject.put("result", gottenUser.getFullUser());
            return new JSONObject(returnObject).toString();
        }

        // No such email found, return null
        returnObject.put("result", null);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Returns single user by giving it's email")
    @CrossOrigin
    @RequestMapping(value = "/user/get/all_by_email_like/{email}", method = RequestMethod.GET, produces = "application/json")
    public String getUserByEmailLike(@PathVariable("email") String gottenEmail) throws JSONException, JsonProcessingException {
        // Use standard return template
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");

        List<Object> users = new ArrayList<>();

        if(StringUtils.isNotEmpty(gottenEmail)){
            for (User user :UserData.getAllUserByEmailLike(gottenEmail)) {
                users.add(user.getMap());
            }

            System.out.println(users);
            returnObject.put("result", users);
            return new JSONObject(returnObject).toString();
        }

        returnObject.put("result", null);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Updates the user to given value's in body using it's ID")
    @CrossOrigin
    @RequestMapping(value = "/user/put/by_id/{id}", method = RequestMethod.PUT, produces = "application/json")
    public String UpdateUser(@PathVariable("id") ObjectId gottenID, @RequestBody User body) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();


        String parsedID = gottenID.toString();
        if(parsedID.length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        User user = UserData.getUserByID(gottenID);
        System.out.println(user.isVerified());

        // Check if given values are not null and correct
        if (body.getFirstName().equals("") || body.getLastName().equals("") || body.getEmail().equals("")) {
            returnObject.put("error", true);
            errorList.add("Een of meerdere velden zijn niet ingevuld");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // check if first and last name do not contain numbers
        if (body.getFirstName().matches(".*\\d.*") || body.getLastName().matches(".*\\d.*")) {
            returnObject.put("error", true);
            errorList.add("Voornaam en achternaam mag geen cijfers bevatten");
        }

        // check if email is valid
        if (UserData.checkEmailValid(body.getEmail())) {
            returnObject.put("error", true);
            errorList.add("Het ingevulde e-mailadres is ongeldig");
        }

        // check if user actually exists
        if(UserData.getUserByID(gottenID) == null){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
        }

        // If there is no error during checking process, update the user with given values
        if(!((Boolean) returnObject.get("error"))){

            user.setFirstName(body.getFirstName());
            user.setLastName(body.getLastName());
            user.setNotifications(body.getNotifications());
            UserData.saveUser(user);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Updates the user's password to given value's in body using it's ID.")
    @CrossOrigin
    @RequestMapping(value = "/user/put/new_password/by_id/{id}", method = RequestMethod.PUT, produces = "application/json")
    public String UpdateUserPassword(@PathVariable("id") ObjectId gottenID, @RequestBody Map<String, String> body) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        if(gottenID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // check if all values filled in
        if(body.get("newPassword").equals("") || body.get("newRepeatPassword").equals("")){
            returnObject.put("error", true);
            errorList.add( "Een of meerdere velden zijn leeg");
        }

        if(!Boolean.parseBoolean(body.get("resetPassword")) && body.get("oldPassword").equals("")){
            returnObject.put("error", true);
            errorList.add("Een of meerdere velden zijn leeg.");
        }

        // check if user actually exists
        if(UserData.getUserByID(gottenID) == null){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // Get user data
        User user = UserData.getUserByID(gottenID);

        // check if old password matches current password if reset is false.
        if(!UserData.checkPassword(body.get("oldPassword"), user.getPassword()) && !Boolean.parseBoolean(body.get("resetPassword"))){
            returnObject.put("error", true);
            errorList.add( "Incorrect wachtwoord");
        }

        // Check if new passwords match
        if(!body.get("newPassword").equals(body.get("newRepeatPassword"))){
            returnObject.put("error", true);
            errorList.add( "Wachtwoorden komen niet overeen");
        }

        // check if new password meets password rules
        String passwordErrResult = UserData.validatePassword(body.get("newPassword"));
        if(!passwordErrResult.equals("")){
            returnObject.put("error", true);
            String[] passwordErrors = passwordErrResult.split(",");
            errorList.addAll(Arrays.asList(passwordErrors)); // add password errors to error list
        }


        //If there is no error during checking process, update the user with new password
        if(!((Boolean) returnObject.get("error"))){
            String encryptedPassword = UserData.encryptPassword(body.get("newPassword"));

            System.out.println(user.getMap());
            System.out.println(user.isVerified());

            user.setPassword(encryptedPassword);
            UserData.saveUser(user);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Updates the user's password to given value's in body using it's Token.")
    @CrossOrigin
    @RequestMapping(value = "/user/put/new_password/by_token/{token}", method = RequestMethod.PUT, produces = "application/json")
    public String ResetUserPassword(@PathVariable("token") UUID gottenToken, @RequestBody Map<String, String> body) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        if(gottenToken.toString().length() != 36){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // check if all values filled in
        if(body.get("newPassword").equals("") || body.get("newRepeatPassword").equals("")){
            returnObject.put("error", true);
            errorList.add( "Een of meerdere velden zijn leeg");
        }
        else if(!Boolean.parseBoolean(body.get("resetPassword")) && body.get("oldPassword").equals("")){
            returnObject.put("error", true);
            errorList.add("Een of meerdere velden zijn leeg.");
        }

        // check if token exists in database
        if(!TokenData.checkTokenExists(gottenToken)){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // get the full token
        Token fullToken = TokenData.getUserIDByToken(gottenToken);

        // check if user actually exists
        if(UserData.getUserByID(fullToken.getUserId()) == null){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // Get user data from token user ID
        User user = UserData.getUserByID(fullToken.getUserId());

        // check if old password matches current password if reset is false.
        if(!UserData.checkPassword(body.get("oldPassword"), user.getPassword()) && !Boolean.parseBoolean(body.get("resetPassword"))){
            returnObject.put("error", true);
            errorList.add( "Incorrect wachtwoord");
        }

        // Check if new passwords match
        if(!body.get("newPassword").equals(body.get("newRepeatPassword"))){
            returnObject.put("error", true);
            errorList.add( "Wachtwoorden komen niet overeen");
        }

        // Check if new password meets password rules
        String passwordErrResult = UserData.validatePassword(body.get("newPassword"));
        if(!passwordErrResult.equals("")){
            returnObject.put("error", true);
            String[] passwordErrors = passwordErrResult.split(",");
            errorList.addAll(Arrays.asList(passwordErrors)); // add password errors to error list
        }


        // If there is no error during checking process, update the user with new password
        if(!((Boolean) returnObject.get("error"))){
            String encryptedPassword = UserData.encryptPassword(body.get("newPassword"));

            // set password to new encrypted password
            user.setPassword(encryptedPassword);

            // Update user and save password
            UserData.saveUser(user);
            TokenData.deleteToken(fullToken);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Removes the user by giving it's ID")
    @CrossOrigin
    @RequestMapping(value = "/user/delete/by_id/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public String RemoveUserProfile(@PathVariable("id") ObjectId gottenID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // check if ID correct length
        String parsedID = gottenID.toString();
        if(parsedID.length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            return new JSONObject(returnObject).toString();
        }

        // check if user actually exists
        if(UserData.getUserByID(gottenID) == null){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
        }

        // If there is no error during checking process, update the user with new password
        if(!((Boolean) returnObject.get("error"))){
            User user = UserData.getUserByID(gottenID);
            UserData.deleteUser(user);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Sends given user email with password reset link.")
    @CrossOrigin
    @RequestMapping(value = "/user/send_password_reset_mail/by_email/{email}", method = RequestMethod.POST, produces = "application/json")
    public String SendPasswordResetMail(@PathVariable("email") String email) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        email = email.toLowerCase();

        // check if email is valid
        if (UserData.checkEmailValid(email)) {
            returnObject.put("error", true);
            errorList.add("Het ingevulde e-mailadres is ongeldig");
        }

        // check if user actually exists
        if(UserData.getUserByEmail(email) == null){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))){
            User user = UserData.getUserByEmail(email);

            // check if user is verified.
            if(user.isVerified()){

                // create new token
                Token newToken;

                // Generate token credentials
                newToken = new Token(UUID.randomUUID(), new ObjectId(user.get_id()), "passwordReset");

                System.out.println(newToken.getMap());

                // if token exists in database, replace token with token that's already in DB
                if(TokenData.getTokenByUserIdAndType(new ObjectId(user.get_id()), "passwordReset") != null) {
                    newToken = TokenData.getTokenByUserIdAndType(new ObjectId(user.get_id()), "passwordReset");
                }

                // save token
                TokenData.saveToken(newToken);
                HashMap values = new HashMap();

                // give all values of user and fulltoken to use in email template.
                values.putAll(user.getMap());
                values.putAll(newToken.getMap());
                values.put("tokenURL", frontendUrl + "reset-password/" + newToken.getTokenId());

                // send to recipient
                mailService.sendHTMLMail(email, "Wachtwoordherstel GathrUp", "ResetPasswordTemplate.html" , values);

                returnObject.put("errormsg", errorList);
                return new JSONObject(returnObject).toString();
            }
            returnObject.put("error", true);
            returnObject.put("type", "verification");
            errorList.add("Uw account is nog niet geverifieerd. Verifieer uw account via uw mail om toegang te krijgen.");
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Sends given user email with verification link.")
    @CrossOrigin
    @RequestMapping(value = "/user/send_account_verification_mail/by_email/{email}", method = RequestMethod.POST, produces = "application/json")
    public String SendAccountVerificationMail(@PathVariable("email") String email) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        email = email.toLowerCase();

        // Check if email is valid
        if (UserData.checkEmailValid(email)) {
            returnObject.put("error", true);
            errorList.add("Het ingevulde e-mailadres is ongeldig");
        }

        // Check if user actually exists
        if(UserData.getUserByEmail(email) == null){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))){
            User user = UserData.getUserByEmail(email);

            Token theToken;

            // Generate token credentials
            theToken = new Token(UUID.randomUUID(), new ObjectId(user.get_id()), "verification");

            // If token exists in database, replace token with token that's already in DB
            if(TokenData.getTokenByUserIdAndType(new ObjectId(user.get_id()), "verification") != null) {
                theToken = TokenData.getTokenByUserIdAndType(new ObjectId(user.get_id()), "verification");
            }

            // Save token
            TokenData.saveToken(theToken);

            HashMap values = new HashMap();

            // Give all values of user and fulltoken to use in email template.
            values.putAll(user.getMap());
            values.putAll(theToken.getMap());
            values.put("tokenURL", frontendUrl + "verify-account/" + theToken.getTokenId());

            // Send to recipient
            mailService.sendHTMLMail(email, "Verificatie GathrUp", "AccountVerificationTemplate.html" , values);

        }
        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Sets verified state to true using tokenId.")
    @CrossOrigin
    @RequestMapping(value = "/user/authenticate/by_token/{tokenId}", method = RequestMethod.POST, produces = "application/json")
    public String AuthenticateUser(@PathVariable("tokenId") UUID tokenId) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if token exists in database
        if(!TokenData.checkTokenExists(tokenId)){
            returnObject.put("error", true);
            errorList.add("Sorry, dit token bestaat niet. Probeer het later opnieuw");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))){
            Token fullToken = TokenData.getUserIDByToken(tokenId);
            User user = UserData.getUserByID(fullToken.getUserId());

            //Set the user to verified, update the user and delete the token
            user.setVerified(true);
            UserData.saveUser(user);
            TokenData.deleteToken(fullToken);
        }
        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


}