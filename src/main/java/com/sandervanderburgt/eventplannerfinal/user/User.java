package com.sandervanderburgt.eventplannerfinal.user;

import com.sandervanderburgt.eventplannerfinal.constraints.ValidPassword;
import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.Map;

public class User {

    @Id
    private ObjectId _id;

    private String firstName;
    private String lastName;
    private String email;

    private boolean notifications;
    private boolean isVerified;

    @ValidPassword
    private String password;


    // Constructors
    public User(){

    }

    public User(ObjectId _id, String firstName, String lastName, String email, String password, boolean notifications, boolean isVerified){
        this._id = _id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.notifications = notifications;
        this.isVerified = isVerified;
    }


    // Getters and setters
    public String get_id() {
        return _id.toHexString();
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<String, Object> getFullUser(){
        Map<String, Object> user = new HashMap<>();
        user.put("id", get_id());
        user.put("firstname", getFirstName());
        user.put("lastname", getLastName());
        user.put("email", getEmail());
        user.put("notifications", getNotifications());
        return user;
    }

    public boolean getNotifications() {
        return notifications;
    }

    public void setNotifications(boolean notifications) {
        this.notifications = notifications;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    @ApiModelProperty(hidden = true)
    public Map<String, Object> getMap(){
        Map<String, Object> userMap = new HashMap<>();
        userMap.put("id", get_id());
        userMap.put("firstname", getFirstName());
        userMap.put("lastname", getLastName());
        userMap.put("email", getEmail());
        return userMap;
    }


}