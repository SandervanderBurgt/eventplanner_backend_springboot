package com.sandervanderburgt.eventplannerfinal.user;

import com.sandervanderburgt.eventplannerfinal.repositories.UserRepository;
import org.bson.types.ObjectId;
import org.omg.CORBA.Environment;
import org.passay.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Component
public class UserData {

    public static String authMessagesPath;

    @Value("${Pass.Ath.messages}")
    public void setAuthMessage(String pam) {
        authMessagesPath = pam;
    }

    private static UserRepository repository;

    @Autowired
    public void setRepository(UserRepository repository) {
        UserData.repository = repository;
    }

    public static User getUserByEmail(String email) {
        return repository.findByEmail(email);
    }

    public static List<User> getAllUserByEmailLike(String email) {
        return repository.findAllByEmailLike(email);
    }

    public static boolean checkEmailExists(String email){
        return getUserByEmail(email) != null;
    }

    public static List<User> getAllUsers() {
        return repository.findAll();
    }

    public static User getUserByID(ObjectId _id) {
        return repository.findBy_id(_id);
    }

    public static void newUser(User user){
        user.set_id(ObjectId.get());
        repository.save(user);
    }

    public static void saveUser(User user){
        repository.save(user);
    }

    public static void deleteUser (User user){
        repository.delete(user);
    }

    public static String encryptPassword(String password){
        BCryptPasswordEncoder encryper = new BCryptPasswordEncoder();
        return encryper.encode(password);
    }

    public static boolean checkPassword(String givenPassword, String hashedPassword){
        return BCrypt.checkpw(givenPassword, hashedPassword);
    }

    public static boolean checkEmailValid(String email){
        return !email.contains("@") || !email.contains(".");
    }

    public static String validatePassword(String password){


        Properties props = new Properties();
        try {
            props.load(new FileInputStream(UserData.authMessagesPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        MessageResolver resolver = new PropertiesMessageResolver(props);

        PasswordValidator validator = new PasswordValidator(resolver,
                // at least 8 characters
                new LengthRule(8, 30),
                // at least one upper-case character
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                // at least one lower-case character
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                // at least one digit character
                new CharacterRule(EnglishCharacterData.Digit, 1),
                // at least one symbol (special character)
                new CharacterRule(EnglishCharacterData.Special, 1),
                // no whitespace
                new WhitespaceRule()
        );

        RuleResult result = validator.validate(new PasswordData(password));
        List<String> messages = validator.getMessages(result);
        String messageTemplate = String.join(",", messages);

        return messageTemplate;
    }

}