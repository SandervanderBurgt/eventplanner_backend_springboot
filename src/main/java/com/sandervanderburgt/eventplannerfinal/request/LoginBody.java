package com.sandervanderburgt.eventplannerfinal.request;

import com.sandervanderburgt.eventplannerfinal.constraints.ValidPassword;

public class LoginBody {
    private String Email;

    @ValidPassword
    private String Password;

    public LoginBody(String email, String password) {
        Email = email;
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }
}
