package com.sandervanderburgt.eventplannerfinal.request;

import com.sandervanderburgt.eventplannerfinal.constraints.ValidPassword;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class RegisterBody {

    @Id
    private ObjectId _id;

    private String firstName;
    private String lastName;
    private String email;

    private boolean notifications;
    private boolean isVerified;

    @ValidPassword
    private String password;

    public RegisterBody() {
    }

    public RegisterBody(ObjectId _id, String firstName, String lastName, String email, boolean notifications, boolean isVerified, String password) {
        this._id = _id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.notifications = notifications;
        this.isVerified = isVerified;
        this.password = password;
    }

    public ObjectId get_id() {
        return _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isNotifications() {
        return notifications;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public String getPassword() {
        return password;
    }
}
