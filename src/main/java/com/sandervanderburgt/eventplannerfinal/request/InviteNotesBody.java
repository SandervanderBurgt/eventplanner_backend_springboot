package com.sandervanderburgt.eventplannerfinal.request;

import org.bson.types.ObjectId;

public class InviteNotesBody {
    private String notes;
    private ObjectId eventID;
    private ObjectId userID;

    public InviteNotesBody() {
    }

    public InviteNotesBody(String notes, ObjectId eventID, ObjectId userID) {
        this.notes = notes;
        this.eventID = eventID;
        this.userID = userID;
    }

    public InviteNotesBody(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public ObjectId getEventID() {
        return eventID;
    }

    public ObjectId getUserID() {
        return userID;
    }
}
