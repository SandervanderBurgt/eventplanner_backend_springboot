package com.sandervanderburgt.eventplannerfinal.request;

import com.sandervanderburgt.eventplannerfinal.todo.Todo;

import java.util.List;

public class AllGivenTodosBody {
    private List<Todo> todo;

    public AllGivenTodosBody() {
    }

    public AllGivenTodosBody(List<Todo> todo) {
        this.todo = todo;
    }

    public List<Todo> getTodo() {
        return todo;
    }

    public void setTodo(List<Todo> todo) {
        this.todo = todo;
    }
}
