package com.sandervanderburgt.eventplannerfinal.request;

import org.bson.types.ObjectId;

public class InviteStatusBody {
    private int status;
    private ObjectId userID;
    private ObjectId eventID;

    public InviteStatusBody() {
    }

    public InviteStatusBody(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public ObjectId getUserID() {
        return userID;
    }

    public ObjectId getEventID() {
        return eventID;
    }
}
