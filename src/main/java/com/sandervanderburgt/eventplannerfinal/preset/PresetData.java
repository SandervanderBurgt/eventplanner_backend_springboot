package com.sandervanderburgt.eventplannerfinal.preset;

import com.sandervanderburgt.eventplannerfinal.repositories.PresetRepository;
import com.sandervanderburgt.eventplannerfinal.repositories.TodoRepository;
import com.sandervanderburgt.eventplannerfinal.todo.Todo;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class PresetData {

    private static PresetRepository repository;

    @Autowired
    public void setRepository(PresetRepository repository) {
        PresetData.repository = repository;
    }

    public static Map<String, Object> newPreset(Preset preset){
        preset.set_id(ObjectId.get());
        repository.save(preset);
        return preset.getFullPreset();
    }

    public static List<Preset> getAllPresetsByUserIDAndType(ObjectId userid, String type){
        return repository.findAllByUserIDAndType(userid, type);
    }


//    public static Map<String, Object> updateTodoItem(Todo todo){
//        repository.save(todo);
//        return todo.getFullToDoItem();
//    }
//
//    public static Todo getTodoItemByID(ObjectId todoItemID){
//        return repository.findBy_id(todoItemID);
//    }
//
//    public static void deleteTodoItem(Todo todoItem){
//        repository.delete(todoItem);
//    }
//
//    public static List<Todo> getAllTodoItemsByEventID(ObjectId eventID){
//        return repository.findAllByEventID(eventID);
//    }
//
//    public static void UpdateTodoItem(Todo todoItem){
//        repository.save(todoItem);
//    }

}
