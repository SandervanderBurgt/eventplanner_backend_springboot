package com.sandervanderburgt.eventplannerfinal.preset;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Preset {

    @Id
    private ObjectId _id;

    private ObjectId userID;
    private String title;
    private Map<String, Object> items;

    private type type;

    public enum type {
        EVENT,
        USER,
        LOCATION
    }

    public Preset() {
    }

    public Preset(ObjectId _id, ObjectId userID, String title, Map<String, Object> items, Preset.type type) {
        this._id = _id;
        this.userID = userID;
        this.title = title;
        this.items = items;
        this.type = type;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public ObjectId getUserID() {
        return userID;
    }

    public void setUserID(ObjectId userID) {
        this.userID = userID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Map<String, Object> getItems() {
        return items;
    }

    public void setItems(Map<String, Object> items) {
        this.items = items;
    }

    public Preset.type getType() {
        return this.type;
    }

    public void setType(Preset.type type) {
        this.type = type;
    }

    @ApiModelProperty(hidden = true)
    public Map<String, Object> getFullPreset() {
        Map<String, Object> presetMap = new HashMap<>();
        presetMap.put("_id", this._id.toString());
        presetMap.put("title", this.title);
        presetMap.put("items", this.items);
        presetMap.put("type", type);
        return presetMap;
    }
}
