package com.sandervanderburgt.eventplannerfinal.preset;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sandervanderburgt.eventplannerfinal.event.Event;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.EnumUtils;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PresetController {


    @ApiOperation("Creates new preset")
    @CrossOrigin
    @RequestMapping(value = "/preset/new", method = RequestMethod.POST, produces = "application/json")
    public String newInvite(@Valid @RequestBody Preset presetContent, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        System.out.println(presetContent.getType());

        // create new preset with given content
        PresetData.newPreset(presetContent);

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Gets all presets by userID")
    @CrossOrigin
    @RequestMapping(value = "/preset/get/by_user_id/{userID}/{type}", method = RequestMethod.GET, produces = "application/json")
    public String getPresetsByUserIDAndType(@PathVariable("userID") ObjectId userID, @PathVariable("type") String type) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if userID is 24 characters long
        if(userID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        if(!EnumUtils.isValidEnum(Preset.type.class, type)){
            returnObject.put("error", true);
            errorList.add("Invalid preset type");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }


        // If no errors occur
        if(!((Boolean) returnObject.get("error"))) {
            // loop through presets, get them in full and add them to presets List
            List<Object> presets = new ArrayList<>();
            for (Preset preset : PresetData.getAllPresetsByUserIDAndType(userID, type)) {
                presets.add(preset.getFullPreset());
            }

            // Return the presets
            returnObject.put("result", presets);
        }


        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


}
