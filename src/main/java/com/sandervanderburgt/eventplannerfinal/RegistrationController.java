package com.sandervanderburgt.eventplannerfinal;

import com.sandervanderburgt.eventplannerfinal.request.RegisterBody;
import com.sandervanderburgt.eventplannerfinal.token.Token;
import com.sandervanderburgt.eventplannerfinal.token.TokenData;
import com.sandervanderburgt.eventplannerfinal.user.User;
import com.sandervanderburgt.eventplannerfinal.user.UserData;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin
@RestController
public class RegistrationController {

    @Autowired
    MailService mailService;

    @Value("${GathrUp.frontend.url}")
    String frontendUrl;;

    @RequestMapping(value = "/user/register", method = RequestMethod.POST, produces = "application/json")
    public Object ValidateUser(@Valid @RequestBody RegisterBody registerBody, BindingResult bindingresult) {
        Map<String, Object> returnObject = new HashMap<>();
        List<String> errorList = new ArrayList<String>();

        returnObject.put("error", false);
        returnObject.put("errormsg", "");


        // Check if given values are not null and correct
        if(registerBody.getFirstName().equals("") || registerBody.getLastName().equals("") || registerBody.getEmail().equals("") || registerBody.getPassword().equals("")){
            returnObject.put("error", true);
            errorList.add("Een of meerdere velden zijn niet ingevuld");
        }
        else{
            // check if first and last name do not contain numbers
            if(registerBody.getFirstName().matches(".*\\d.*") || registerBody.getLastName().matches(".*\\d.*")){
                returnObject.put("error", true);
                errorList.add("Voornaam en achternaam mag geen cijfers bevatten");
            }

            // check if email is valid
            if(checkEmailValid(registerBody.getEmail())) {
                returnObject.put("error", true);
                errorList.add("Het ingevulde e-mailadres is ongeldig");
            }

            // check if user exists
            if(UserData.checkEmailExists(registerBody.getEmail().toLowerCase())){
                returnObject.put("error", true);
                errorList.add("Een account met dit e-mailadres bestaat al");
            }

            // check for password errors
            if(bindingresult.hasErrors()){
                returnObject.put("error", true);
                String[] passwordErrors = bindingresult.getFieldError().getDefaultMessage().split(",");
                errorList.addAll(Arrays.asList(passwordErrors)); // add password errors to error list
            }

            if(!((Boolean) returnObject.get("error"))){
                String encryptedPassword = UserData.encryptPassword(registerBody.getPassword());

                User user = new User();
                user.setEmail(registerBody.getEmail().toLowerCase());
                user.setFirstName(registerBody.getFirstName());
                user.setLastName(registerBody.getLastName());
                user.setNotifications(registerBody.isNotifications());
                user.setVerified(false);
                user.setPassword(encryptedPassword);

                UserData.newUser(user);
                returnObject.put("error", false);
                errorList.add("New user created sucessfully");


                Token newToken = new Token(UUID.randomUUID(), new ObjectId(user.get_id()), "verification");
                TokenData.saveToken(newToken);

                HashMap values = new HashMap();
                // give all values of user and fulltoken to use in email template.
                values.putAll(user.getMap());
                values.putAll(newToken.getMap());
                values.put("tokenURL", frontendUrl + "verify-account/" + newToken.getTokenId());

                mailService.sendHTMLMail(user.getEmail(), "Verificatie GathrUp", "AccountVerificationTemplate.html", values);
            }
        }
        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    private boolean checkEmailValid(String email){
        return !email.contains("@") || !email.contains(".");
    }

}
