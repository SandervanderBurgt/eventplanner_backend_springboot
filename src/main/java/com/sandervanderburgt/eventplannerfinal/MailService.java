package com.sandervanderburgt.eventplannerfinal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Value( "${spring.mail.username}" )
    String propEmail;

    public void sendSimpleMessage(String to, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        try {
            message.setFrom(String.valueOf(new InternetAddress(propEmail, "GathrUp")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    public void sendHTMLMail(String to, String subject, String template, Map<String, Object> values) {

        Context context = new Context();
        values.forEach((k, v) -> {
            context.setVariable(k, v);
        });

        String content = templateEngine.process(template, context);

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.ISO_8859_1.name());

            try {
                messageHelper.setFrom(String.valueOf(new InternetAddress(propEmail, "GathrUp")));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(content, true);
        };

        javaMailSender.send(messagePreparator);
    }

}
