package com.sandervanderburgt.eventplannerfinal.event;

import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Event {
    @Id
    private ObjectId _id;

    private ObjectId owner;

    private String title, description, image, location, timeFrom, timeTill;
    private Date date;

    private Double lat, lng;


    public Event() {
    }

    public Event(ObjectId _id, ObjectId owner, String title, String description, String image, Date date, String timeFrom, String timeTill, String location, double lat, double lng) {
        this._id = _id;
        this.owner = owner;
        this.title = title;
        this.description = description;
        this.image = image;
        this.date = date;
        this.timeFrom = timeFrom;
        this.timeTill = timeTill;
        this.location = location;
        this.lat = lat;
        this.lng = lng;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTill() {
        return timeTill;
    }

    public void setTimeTill(String timeTill) {
        this.timeTill = timeTill;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ObjectId getOwner() {
        return owner;
    }

    public void setOwner(ObjectId owner) {
        this.owner = owner;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    @ApiModelProperty(hidden = true)
    public Map<String, Object> getMap() {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-YYYY");
        Map<String, Object> eventMap = new HashMap<>();
        eventMap.put("_id", this._id.toString());
        eventMap.put("title", this.title);
        eventMap.put("description", this.description);
        eventMap.put("image", this.image);
        eventMap.put("date", dateformat.format(this.date));
        eventMap.put("timeFrom", this.timeFrom);
        eventMap.put("timeTill", this.timeTill);
        eventMap.put("location", this.location);
        eventMap.put("owner", this.owner.toString());
        eventMap.put("lat", this.lat);
        eventMap.put("lng", this.lng);
        return eventMap;
    }
}
