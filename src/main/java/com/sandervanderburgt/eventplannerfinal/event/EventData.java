package com.sandervanderburgt.eventplannerfinal.event;

import com.sandervanderburgt.eventplannerfinal.repositories.EventRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EventData {

    private static EventRepository repository;

    @Autowired
    public void setRepository(EventRepository repository) {
        EventData.repository = repository;
    }

    public static Map<String, Object> newEvent(Event event){
        event.set_id(ObjectId.get());
        repository.save(event);
        return event.getMap();
    }

    public static Map<String, Object> saveEvent(Event event){
        repository.save(event);
        return event.getMap();
    }

    public static Event getEventByID(ObjectId eventID){
        return repository.findBy_id(eventID);
    }

    public static List<Event> getEventByOwnedID(ObjectId userID){
        return repository.findAllByOwner(userID);
    }

    public static void deleteEventByID(ObjectId eventID){
        Event theEvent = EventData.getEventByID(eventID);
        repository.delete(theEvent);
    }
}
