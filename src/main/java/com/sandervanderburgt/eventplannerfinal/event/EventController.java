package com.sandervanderburgt.eventplannerfinal.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sandervanderburgt.eventplannerfinal.MailService;
import com.sandervanderburgt.eventplannerfinal.invite.Invite;
import com.sandervanderburgt.eventplannerfinal.invite.InviteData;
import com.sandervanderburgt.eventplannerfinal.token.TokenData;
import com.sandervanderburgt.eventplannerfinal.user.User;
import com.sandervanderburgt.eventplannerfinal.user.UserData;
import io.swagger.annotations.ApiOperation;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.util.ArrayUtils;
import org.thymeleaf.util.DateUtils;

import javax.validation.Valid;
import java.util.*;

@RestController
public class EventController {

    @Autowired
    MailService mailService;

    @Value("${GathrUp.frontend.url}")
    String frontendUrl;

    @ApiOperation("Creates new event")
    @CrossOrigin
    @RequestMapping(value = "/event/new", method = RequestMethod.POST, produces = "application/json")
    public String NewEvent(@Valid @RequestBody Event event, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // check if any of the fields are empty
        if(StringUtils.isAnyEmpty(event.getTitle(), event.getDescription(), event.getImage(), event.getLocation(), event.getOwner().toString())){
            returnObject.put("error", true);
            errorList.add("Één of meerdere velden zijn leeg gelaten! Probeer het A.U.B. opnieuw");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            returnObject.put("result", EventData.newEvent(event));
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Delete's event by its ID")
    @CrossOrigin
    @RequestMapping(value = "/event/delete/by_id/{eventID}", method = RequestMethod.DELETE, produces = "application/json")
    public String deleteEventByID(@PathVariable("eventID") ObjectId eventID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if the eventID is actually 24 characters
        if(eventID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Delete the event with the given eventID
            EventData.deleteEventByID(eventID);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }



    @ApiOperation("Updates event by it's id and sends emails of change to all invited")
    @CrossOrigin
    @RequestMapping(value = "/event/update", method = RequestMethod.PUT, produces = "application/json")
    public String updateEvent(@Valid @RequestBody Event event, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // check if any of the fields are empty
        if(StringUtils.isAnyEmpty(event.getTitle(), event.getDescription(), event.getImage(), event.getLocation(), event.getOwner().toString())){
            returnObject.put("error", true);
            errorList.add("Één of meerdere velden zijn leeg gelaten! Probeer het A.U.B. opnieuw");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {

            // Check if anything has been updated
            if(!event.getMap().equals( EventData.getEventByID(event.get_id()).getMap())){

                // Get list with people and send emails
                for (Invite invite :InviteData.getAllInvitesByEventID(event.get_id())) {
                    User user = UserData.getUserByEmail(invite.getEmail());

                    // if the user has notifications enabled
                    if(user.getNotifications()){
                        HashMap values = new HashMap();
                        // Give all values of user and fulltoken to use in email template.
                        values.putAll(user.getMap());
                        values.putAll(event.getMap());
                        values.put("eventurl",  frontendUrl + "event/" + invite.getEventID());

                        // Send the mail using mailservice
                        mailService.sendHTMLMail(invite.getEmail(), "Een van uw afspraken is gewijzigd!", "eventChangesMadeTemplate.html", values);
                    }
                }
                // Update the event
                EventData.saveEvent(event);
            }
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Gets all events the user has or is invited to")
    @CrossOrigin
    @RequestMapping(value = "/event/get/by_id/{eventID}", method = RequestMethod.GET, produces = "application/json")
    public String getEventByID(@PathVariable("eventID") ObjectId eventID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if eventID is actually 24 characters
        if(eventID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // return full event
            returnObject.put("result", EventData.getEventByID(eventID).getMap());
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Gets all events the user has or is invited to")
    @CrossOrigin
    @RequestMapping(value = "/event/get/by_owner/{ownerID}", method = RequestMethod.GET, produces = "application/json")
    public String getEventsByOwner(@PathVariable("ownerID") ObjectId ownerID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if ownerID is 24 characters long
        if(ownerID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Prepare list for containing owned events
            List<Object> events = new ArrayList<>();
            // Loop through owned events and add to map
            for (Event event :EventData.getEventByOwnedID(ownerID)) {
                events.add(event.getMap());
            }
            // Return map with owned events
            returnObject.put("result", events);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }
}
