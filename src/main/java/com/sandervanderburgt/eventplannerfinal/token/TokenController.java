package com.sandervanderburgt.eventplannerfinal.token;

import io.swagger.annotations.ApiOperation;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class TokenController {

//    @ApiOperation("Gets the user using valid tokenid")
//    @CrossOrigin
//    @RequestMapping(value = "/token/getUser/byToken/{token}", method = RequestMethod.GET, produces = "application/json")
//    public String getUserDetailsFromToken(@PathVariable("token") String token) throws Exception {
//        Map<String, Object> returnObject = new HashMap<>();
//        returnObject.put("error", false);
//        returnObject.put("errormsg", "");
//        List<String> errorList = new ArrayList<String>();
//
//        if(TokenData.getUserIDByToken(UUID.fromString(token)) == null){
//            returnObject.put("error", true);
//            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
//        }
//
//        if(!((Boolean) returnObject.get("error"))){
//            Token fullToken = TokenData.getUserIDByToken(UUID.fromString(token));
//            User user = UserData.getUserByID(fullToken.getUserId());
//            returnObject.put("result", user.getFullUser());
//        }
//
//        returnObject.put("errormsg", errorList);
//        return new JSONObject(returnObject).toString();
//    }

//    @ApiOperation("Creates a new password reset token using a valid email that has been registered")
//    @CrossOrigin
//    @RequestMapping(value = "/token/createToken/byEmail/{email}", method = RequestMethod.POST, produces = "application/json")
//    public String GeneratePasswordReset(@PathVariable("email") String email, @RequestBody Map<String, String> body) throws Exception {
//        Map<String, Object> returnObject = new HashMap<>();
//        returnObject.put("error", false);
//        returnObject.put("errormsg", "");
//        List<String> errorList = new ArrayList<String>();
//
//        // check if email is valid
//        if (UserData.checkEmailValid(email)) {
//            returnObject.put("error", true);
//            errorList.add("Het ingevulde e-mailadres is ongeldig");
//        }
//
//        // check if user actually exists
//        if (UserData.getUserByEmail(email) == null) {
//            returnObject.put("error", true);
//            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
//        }
//
//        // If there is no error during checking process, update the user with new password
//        if (!((Boolean) returnObject.get("error"))) {
//            User user = UserData.getUserByEmail(email);
//
//            // If the token with the given type already exists in the token collection
//            if(TokenData.getTokenByUserIdAndType(new ObjectId(user.get_id()), body.get("type")) != null){
//                returnObject.put("error", true);
//                errorList.add("Er bestaat al een token met dit type. Kan geen nieuwe token aanmaken.");
//            }
//
//            if (!((Boolean) returnObject.get("error"))) {
//                Token token = new Token(UUID.randomUUID(), new ObjectId(user.get_id()), body.get("type"));
//                TokenData.newToken(token);
//            }
//
//        }

    @ApiOperation("Checks if the token exists. Returns error if token doesn't exist")
    @CrossOrigin
    @RequestMapping(value = "/token/checkExists/byToken/{tokenId}", method = RequestMethod.POST, produces = "application/json")
    public String CheckTokenExists(@PathVariable("tokenId") UUID tokenId) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // check if token exists in database
        if(!TokenData.checkTokenExists(tokenId)){
            returnObject.put("error", true);
            errorList.add("Sorry, dit token bestaat niet. Probeer het later opnieuw");
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }
}
