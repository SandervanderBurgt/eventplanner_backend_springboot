package com.sandervanderburgt.eventplannerfinal.token;

import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Token {

    @Id
    private ObjectId _id;

    private UUID tokenId;
    private ObjectId userId;

    private String type;

    public Token(UUID tokenId, ObjectId userId, String type) {
        this.tokenId = tokenId;
        this.userId = userId;
        this.type = type;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public UUID getTokenId() {
        return tokenId;
    }

    public void setTokenId(UUID tokenId) {
        this.tokenId = tokenId;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @ApiModelProperty(hidden = true)
    public Map<String, Object> getMap(){
        Map<String, Object> tokenMap = new HashMap<>();
        tokenMap.put("tokenid", getTokenId());
        tokenMap.put("userid", getUserId());
        tokenMap.put("type", getType());
        return tokenMap;
    }
}
