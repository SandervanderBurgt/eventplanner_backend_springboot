package com.sandervanderburgt.eventplannerfinal.token;

import com.sandervanderburgt.eventplannerfinal.repositories.TokenRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TokenData {

    private static TokenRepository repository;

    @Autowired
    public void setRepository(TokenRepository repository) {
        TokenData.repository = repository;
    }

    public static void saveToken(Token token){
        repository.save(token);
    }

    public static Token getUserIDByToken(UUID token) {
        return repository.findByTokenId(token);
    }

    public static Token getTokenByUserIdAndType(ObjectId userId, String type) {
        return repository.findByUserIdAndType(userId, type);
    }

    public static boolean checkTokenExists(UUID token){
        return getUserIDByToken(token) != null;
    }

    public static void deleteToken (Token token){
        repository.delete(token);
    }


}
