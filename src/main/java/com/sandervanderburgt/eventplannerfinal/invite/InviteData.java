package com.sandervanderburgt.eventplannerfinal.invite;

import com.sandervanderburgt.eventplannerfinal.repositories.InviteRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InviteData {

    private static InviteRepository repository;

    @Autowired
    public void setRepository(InviteRepository repository) {
        InviteData.repository = repository;
    }

    public static void newInvite(Invite invite){
        invite.set_id(ObjectId.get());
        repository.save(invite);
    }

    public static void updateInvite(Invite invite){
        repository.save(invite);
    }

    public static void deleteInvite(Invite invite){
        repository.delete(invite);
    }

    public static void deleteAllInvitesByEventID(ObjectId eventID){
        repository.deleteAllByEventID(eventID);
    }

    public static Invite getInviteByEventAndUser(ObjectId eventID, String email){
        return repository.findByEventIDAndEmail(eventID, email);
    }

    public static List<Invite> getAllInvitesByEmail(String email){
        return repository.findAllByEmail(email);
    }

    public static List<Invite> getAllInvitesByEventID(ObjectId eventID){
        return repository.findAllByEventID(eventID);
    }

}