package com.sandervanderburgt.eventplannerfinal.invite;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sandervanderburgt.eventplannerfinal.MailService;
import com.sandervanderburgt.eventplannerfinal.event.Event;
import com.sandervanderburgt.eventplannerfinal.event.EventData;
import com.sandervanderburgt.eventplannerfinal.request.InviteNotesBody;
import com.sandervanderburgt.eventplannerfinal.request.InviteStatusBody;
import com.sandervanderburgt.eventplannerfinal.user.User;
import com.sandervanderburgt.eventplannerfinal.user.UserData;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class InviteController {

    @Autowired
    MailService mailService;

    @Value("${GathrUp.frontend.url}")
    String frontendUrl;;

    @ApiOperation("Creates new invite")
    @CrossOrigin
    @RequestMapping(value = "/invite/new", method = RequestMethod.POST, produces = "application/json")
    public String newInvite(@Valid @RequestBody Invite invite, BindingResult bindingresult) throws Exception {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if any of the fields are empty
        if(StringUtils.isAnyEmpty(invite.getEmail(), invite.getEventID().toString())){
            returnObject.put("error", true);
            errorList.add("Één of gebruikers konden niet worden toegevoegd. Probeer het later opnieuw");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Get user and event
            User user = UserData.getUserByEmail(invite.getEmail());
            Event event = EventData.getEventByID(invite.getEventID());

            InviteData.newInvite(invite);

            if(user.getNotifications()){
                // Put user, event and eventurl in a map for template
                HashMap values = new HashMap();
                // Give all values of user and fulltoken to use in email template.
                values.putAll(user.getMap());
                values.putAll(event.getMap());
                values.put("eventurl",  frontendUrl + "event/" + invite.getEventID());

                // Create new invite and send invite email to user.
                mailService.sendHTMLMail(invite.getEmail(), "U bent uitgenodigd voor een evenement!", "EventInviteTemplate.html", values);
            }
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Delete's an invite")
    @CrossOrigin
    @RequestMapping(value = "/invite/delete/by_email_and_event_id/{email}/{eventid}", method = RequestMethod.DELETE, produces = "application/json")
    public String deleteInvite (@PathVariable("email") String email, @PathVariable("eventid") ObjectId eventID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();


        // Check if any of the fields are empty or incorrect
        if(StringUtils.isEmpty(email) || eventID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Één of gebruikers konden niet worden verwijderd. Probeer het later opnieuw");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            Invite invite = InviteData.getInviteByEventAndUser(eventID, email);
            InviteData.deleteInvite(invite);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Gets all user's invites by it's ID")
    @CrossOrigin
    @RequestMapping(value = "/invite/get/by_user_id/{userID}", method = RequestMethod.GET, produces = "application/json")
    public String getInvitesByUserID(@PathVariable("userID") ObjectId userID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if userID is 24 characters long
        if(userID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Get the user
            User user = UserData.getUserByID(userID);

            // Get all the user's invites and add them to the List
            List<Object> invites = new ArrayList<>();
            for (Invite invite :InviteData.getAllInvitesByEmail(user.getEmail())) {
                invites.add(invite.getFullInvite());
            }

            // Return the invites
            returnObject.put("result", invites);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Set's the user's status for the specified event to the specified status")
    @CrossOrigin
    @RequestMapping(value = "/invite/put/status/", method = RequestMethod.PUT, produces = "application/json")
    public String setInviteStatus(@RequestBody InviteStatusBody status) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if userID and eventID are 24 characters long
        if(status.getUserID().toString().length() != 24 || status.getEventID().toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            User user = UserData.getUserByID(status.getUserID());
            Invite invite = InviteData.getInviteByEventAndUser(status.getEventID(), user.getEmail());

            invite.setStatus(status.getStatus());
            InviteData.updateInvite(invite);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Set's the user's notes for the specified event to the specified note in the body")
    @CrossOrigin
    @RequestMapping(value = "/invite/put/notes", method = RequestMethod.PUT, produces = "application/json")
    public String setInviteNotes(@RequestBody InviteNotesBody notes) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if eventID and userID are actually 24 characters long
        if(notes.getEventID().toString().length() != 24 || notes.getUserID().toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            User user = UserData.getUserByID(notes.getUserID());
            Invite invite = InviteData.getInviteByEventAndUser(notes.getEventID(), user.getEmail());

            invite.setNotes(notes.getNotes());
            InviteData.updateInvite(invite);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }

    @ApiOperation("Set's the user's notes for the specified event to the specified note in the body")
    @CrossOrigin
    @RequestMapping(value = "/invite/delete/all_by_event_id/{eventID}", method = RequestMethod.DELETE, produces = "application/json")
    public String deleteInvitesByEventID(@PathVariable("eventID") ObjectId eventID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if the eventid is 24 characters long
        if(eventID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Get all invites of the event
            List<Invite> invites = InviteData.getAllInvitesByEventID(eventID);

            // Loop through the invites
            for (Invite invite: invites) {
                // Get the userid and event
                User user = UserData.getUserByEmail(invite.getEmail());
                Event event = EventData.getEventByID(eventID);

                // If the user has notifications enabled
                if(user.getNotifications()){
                    // Create map with values for mail template
                    HashMap values = new HashMap();
                    // Give all values of user and fulltoken to use in email template.
                    values.putAll(user.getMap());
                    values.putAll(event.getMap());

                    // Send mail to invitee informing about cancelled event
                    mailService.sendHTMLMail(invite.getEmail(), "Een van uw afspraken is geannuleerd", "EventCancelTemplate.html", values);
                }
            }
            // Remove all invites from the event
            InviteData.deleteAllInvitesByEventID(eventID);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }


    @ApiOperation("Gets one invite by eventID and userID")
    @CrossOrigin
    @RequestMapping(value = "/invite/get/by_event_id/{eventID}", method = RequestMethod.GET, produces = "application/json")
    public String getInvitesByEventID(@PathVariable("eventID") ObjectId eventID) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");
        List<String> errorList = new ArrayList<String>();

        // Check if eventID is 24 characters long
        if(eventID.toString().length() != 24){
            returnObject.put("error", true);
            errorList.add("Er is iets fout gegaan, probeer het later opnieuw!");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }

        // If there is no error during checking process
        if(!((Boolean) returnObject.get("error"))) {
            // Create new list for the invites
            List<Object> invites = new ArrayList<>();

            // Loop through all invites of event and add to new List
            for (Invite invite :InviteData.getAllInvitesByEventID(eventID)) {
                invites.add(invite.getFullInvite());
            }

            // Return all invites of the event
            returnObject.put("result", invites);
        }

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }
}
