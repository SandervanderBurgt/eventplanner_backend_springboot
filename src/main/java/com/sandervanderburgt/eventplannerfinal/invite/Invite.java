package com.sandervanderburgt.eventplannerfinal.invite;

import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.HashMap;
import java.util.Map;

public class Invite {
    @Id
    private ObjectId _id;

    private ObjectId eventID;
    private String email, notes;

    private int status;

    public Invite(){
    }

    public Invite(ObjectId _id,  String userEmail, ObjectId eventID, int status, String notes) {
        this._id = _id;
        this.email = userEmail;
        this.eventID = eventID;
        this.status = status;
        this.notes = notes;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ObjectId getEventID() {
        return eventID;
    }

    public void setEventID(ObjectId eventID) {
        this.eventID = eventID;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @ApiModelProperty(hidden = true)
    public Map<String, Object> getFullInvite() {
        Map<String, Object> inviteMap = new HashMap<>();
        inviteMap.put("_id", this._id.toString());
        inviteMap.put("email", this.email);
        inviteMap.put("eventID", this.eventID.toString());
        inviteMap.put("status", this.status);
        inviteMap.put("notes", this.notes);
        return inviteMap;
    }
}
