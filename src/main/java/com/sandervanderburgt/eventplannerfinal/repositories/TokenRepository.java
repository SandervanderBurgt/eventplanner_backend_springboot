package com.sandervanderburgt.eventplannerfinal.repositories;

import com.sandervanderburgt.eventplannerfinal.token.Token;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface TokenRepository extends MongoRepository<Token, String> {
    Token findByTokenId(UUID _id);
    Token findByUserIdAndType(ObjectId userId, String type);
}
