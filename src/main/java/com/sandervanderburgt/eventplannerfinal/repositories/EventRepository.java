package com.sandervanderburgt.eventplannerfinal.repositories;

import com.sandervanderburgt.eventplannerfinal.event.Event;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import javax.validation.constraints.NotEmpty;
import java.util.List;

public interface EventRepository extends MongoRepository<Event, String> {
    Event findBy_id(ObjectId _id);
    List<Event> findAllByOwner(ObjectId ownerID);
//    List<Event> findAllByOwnerOrInvitees(ObjectId owner, String invitees);
}
