package com.sandervanderburgt.eventplannerfinal.repositories;

import com.sandervanderburgt.eventplannerfinal.todo.Todo;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TodoRepository extends MongoRepository<Todo, String> {
    List<Todo> findAllByEventID(ObjectId eventID);
    Todo findBy_id(ObjectId todoItemID);
    void deleteBy_id(ObjectId todoItemID);
}
