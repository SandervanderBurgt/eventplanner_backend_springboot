package com.sandervanderburgt.eventplannerfinal.repositories;

import com.sandervanderburgt.eventplannerfinal.preset.Preset;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PresetRepository extends MongoRepository<Preset, String> {
    List<Preset> findAllByUserIDAndType(ObjectId userID, String type);
}
