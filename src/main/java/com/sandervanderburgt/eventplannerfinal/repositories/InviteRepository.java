package com.sandervanderburgt.eventplannerfinal.repositories;

import com.sandervanderburgt.eventplannerfinal.invite.Invite;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface InviteRepository extends MongoRepository<Invite, String> {
    List<Invite> findAllByEmail(String email);
    Invite findByEventIDAndEmail(ObjectId eventID, String Email);
    List<Invite> findAllByEventID(ObjectId eventID);
    void deleteAllByEventID(ObjectId eventID);
}
