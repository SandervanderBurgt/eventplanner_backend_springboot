package com.sandervanderburgt.eventplannerfinal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sandervanderburgt.eventplannerfinal.request.LoginBody;
import com.sandervanderburgt.eventplannerfinal.user.User;
import com.sandervanderburgt.eventplannerfinal.user.UserData;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class LoginController {
    @RequestMapping(value = "/user/login", method = RequestMethod.POST, produces = "application/json")
    public Object LoginUser(@RequestBody LoginBody loginBody) throws JSONException, JsonProcessingException {
        Map<String, Object> returnObject = new HashMap<>();
        List<String> errorList = new ArrayList<String>();
        returnObject.put("error", false);
        returnObject.put("errormsg", "");

        if(!loginBody.getEmail().equals("") && !loginBody.getPassword().equals("")){
            if(UserData.checkEmailExists(loginBody.getEmail().toLowerCase())){
                User gottenUser = UserData.getUserByEmail(loginBody.getEmail().toLowerCase());
                if(gottenUser.isVerified()){
                    if(UserData.checkPassword(loginBody.getPassword(), gottenUser.getPassword())){
                        Map<String, Object> user = new HashMap<>();
                        user.put("id", gottenUser.get_id());
                        returnObject.put("result", user);
                        returnObject.put("errormsg", errorList);
                        return new JSONObject(returnObject).toString();
                    }
                    returnObject.put("error", true);
                    errorList.add("email or password incorrect");
                    returnObject.put("errormsg", errorList);
                    return new JSONObject(returnObject).toString();
                }
                returnObject.put("error", true);
                returnObject.put("type", "verification");
                errorList.add("Uw account is nog niet geverifieerd. Verifieer uw account via uw mail om toegang te krijgen.");
                returnObject.put("errormsg", errorList);
                return new JSONObject(returnObject).toString();
            }
            returnObject.put("error", true);
            errorList.add("email or password incorrect");
            returnObject.put("errormsg", errorList);
            return new JSONObject(returnObject).toString();
        }
        returnObject.put("error", true);
        errorList.add("One or more fields are not filled in. Please fill all fields");

        returnObject.put("errormsg", errorList);
        return new JSONObject(returnObject).toString();
    }
}
