package com.sandervanderburgt.eventplannerfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.sandervanderburgt")
public class EventplannerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventplannerApplication.class, args);
    }

}
